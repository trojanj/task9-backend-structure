### Achievement service

This service will have the task for sending an achievement notification and saving user achievement progress.  
For achievement notification will be used WebSocket protocol.  
Achievement database will store achievements notification and users achievements data.  
On the client side will be page for customers with their achievements progress. Also, there will be page for an administrator to add, edit, delete kinds of achievements.

#### Structure description

`api` - routes for achievements.  
`repository` - query’s to the database.  
`models` - achievement, notification model.  
`socket` - sockets for achievement notifications.
