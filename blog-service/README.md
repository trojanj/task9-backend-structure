### Blog service

Blog service provides api for posts and comments.  
Only administrator has privileges to publish new post int the blog. 
All authenticated users can comment blog posts.  
Blog service interacts with Marketing service. Users get notification about new posts on email.

#### Structure description

`api` - routes for posts and comments   
`repository` - queries posts and comments    
`models` - models for post, comment
   

