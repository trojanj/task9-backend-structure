### Marketing service

This service will have the task for sending an email or web notification.  
For web notification will be used WebSocket protocol.  
Marketing database will store notifications data. Only administrator will have access to manage it.  
On the client side will be page for managing notifications, so this service will provide api for that.

#### Structure description

`api` - routes for notifications.  
`repository` - finding, saving, updating user query’s to the database.  
`models` - notification model.  
`socket` - sockets for web notifications.
