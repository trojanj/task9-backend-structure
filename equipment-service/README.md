### Equipment service

Equipment service provides api for managing rent catalog, sale catalog and booking equipment.    
Only administrator has privileges to manage equipment.   
All authenticated users can book equipment and pay for it.    
Equipment service interacts with Payment service for providing payment.  

#### Structure description

`api` - routes for equipment   
`repository` - operations with equipment in db 
`models` - equipment model
   

