### Bar service

Bar service gives access to api in depend on user role.  
Barmen and admin has access to add an assortment and choose a menu. Customers and barmen can make orders.  
For paying orders Bar service interacts with Payment service. For notification about result of the operation it interacts with Marketing service. 

#### Structure description

`api` - routes for an assortment, menu, orders; validation, error handling middlewares etc.   
`repository` - repositories for an assortment, menu and orders  
`models` - models for assortmentItem, order. Menu will consist of assortment items.
   

