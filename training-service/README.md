### Training service

Training service provides api for buying abonements. Training service interacts with Payment service for providing payment.  
Images from training store on third party service. Links for it store in the database.

#### Structure description

`api` - training routes  
`api/services` - methods for working with third party service and repository   
`repository` - queries to db    
`models` - image
