### Payment service

Payment service provides api for making payment operations. It works using external api for conducting payment transactions.  
Payment database store all invoices about completed transactions. Users can always receive it by sending request from the user account.  
Besides, database stores actual promo codes, which is added by an administrator.  
Users have opportunity deposit money into an account and then pay for every business service.

#### Structure description

`api` - routes for payments  
`api/services` - methods for working with repositories and third party api   
`repository` - queries to db    
`models` - invoice model
