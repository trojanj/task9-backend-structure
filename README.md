## Microservices architecture

### Why Microservices?

1. There is need to develop MVP fast. Different team can work on different services independently.
2. There is need to add new features and scale application in the future. With Microservices, it will be easier to design new features and implement changes in the current architecture.
3. There are independent components like blog and payment, bar and equipment etc. In microservices architecture component failures cause only the absence of a specific functionality.
4. Microservices aren't tied to the technologies used in other services. So it provides opportunity to use the best fitting technologies.
5. Microservices are easier to keep modular. Technically, this is ensured by rigid boundaries between individual services.
6. Smaller units are easy to understand, maintain and update.

### Protocols

Almost all services will use HTTP protocol, except marketing and achievement services, which must have persistent connection between a client and server. These two will use a WebSocket protocol. 

### Services structure

The most of the services will have the same structure parts:
- api - routes, middlewares and services for our apis
- config - config for the app
- models - models for data
- repository - abstraction over our db
- server - server setup code
- package.json - dependencies
- index.js - entry point of the app


