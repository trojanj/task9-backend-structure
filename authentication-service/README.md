### Authentication service

Authentication service determines if an authenticated user has rights to access an API's endpoint. 
We will use JSON Web Tokens to exchange data about authorized users between services, so we need a pair of private and public keys.  
We should place private.key in the `/authentication-service` directory because it will be used only by this service to sign tokens. The public.key file should be stored in the root directory because it will be used by all other services which need to check token signatures with this public key.  
The application will have 4 roles: customer, barmen, coach and admin. That's why JWT should contain strings indicating what privileges are given to the user.  
Services, which interact with client side directly will have JWT validation for protecting API endpoints.

#### Structure description

`api` - routes for log in and sign up, validation middleware for sign up etc.  
`config` - configs for the server, jwt, db   
`repository` - finding, saving user query’s to the database etc.  
`models` - user model



